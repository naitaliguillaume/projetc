#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void cryptage_msg()
{
	FILE *fichier;
    FILE *fic;
    //declarations locale:
	int taille_peroq= 4, taille_msg;
	char message[256]="";
	char cle[5];

	/* Recuperer la cle  dans le fichier peroq.def */
 	fichier = fopen("peroq.def", "r");  /* read */

	if (fichier != NULL)
    {
		fgets(cle,5, fichier); // On lit maximum 5 caractères du fichier, on stocke le tout dans "cle"
	}
	fclose(fichier);


	/* Recuperer le msg dans le fichier source.txt */
 	fichier = fopen("annuaire.txt", "r");  /* read */
	if (fichier != NULL)
    {
		// recupère la longueur du msg
		fgets(message,256, fichier); // On lit maximum 256 caractères du fichier, on stocke le tout dans "message"
		taille_msg = strlen(message);
	}
	fclose(fichier);


	int a,b,s=0;
	int j=0;
	//Parcourer du message à aditionner avec le code tournant par exemple abcd
	//ce qui permet le cryptage du message:
	for(int i=0;i<(taille_msg);i++)
	{
		
		if(j<4) // cle de longueur 4
		{
			a =(int)cle[j];
			//printf("%d ",a);
			b =(int)message[i];
			s=(b-a);
			//printf("%d ",b);
			//printf("%d\n",s);
			if(i==0)
			{
				fic = fopen("annuaire.crt", "w");  /* Ecrit et efface tout le contenu de dest.crt*/
				fprintf(fic,"%06d",s); //affichage sur 6 digit décimal
				fclose(fic);
			}
			else
			{
				fic = fopen("annuaire.crt", "a+");  /* Ecrit et ajoute du contenu à la suite dans le fichier dest.crt*/
				fprintf(fic,"%06d",s);
				fclose(fic);
			}	

			j=j+1;
			if (j==4)
			{
				j=0; //remise à zéro du conteur position cle
			}

		}
	}
	//system("cd /home/guitus/projetc"); //commandes système pour ce mettre dans le repertoire courant
	//system("rm source.txt"); //commandes sysstème pour supprimer le fichier source.txt

}

void decryptage_msg()
{
	//déclaration des variables locales:
	FILE *fichier;
    FILE *fic;
    FILE *fic2;
	int taille_peroq= 4;
	int taille_message;
	char message[256];
	char cle[5];

	/* Recuperer la cle  dans le fichier peroq.def */
 	fichier = fopen("peroq.def", "r");  /* read */

	if (fichier != NULL)
    {
		fgets(cle,5, fichier); // On lit maximum 5 caractères du fichier, on stocke le tout dans "cle"
	}
	fclose(fichier);


 	fic = fopen("annuaire.crt", "r");  /* lecture du fichier crypté*/
 	fic2 = fopen("annuaire.drt", "w");  /* ouverture du fichier décrypté pour le remplir du message décrypté*/
 	int i=0; //variable de parcour du fichier crypté
 	int j=1; //variable de parcour de la cle
 	int somme=0; //varial pour le décryptage

 	fscanf (fic, "%06d", &i); // récupére 6 decimals par 6 decimals
 	//printf ("%030d ", i);   
 	somme=i+(int) cle[0]; // calcul du decryptatge 
 	fprintf(fic2, "%c", somme);
  	while (!feof (fic)) // parcour du fichier 6 decimals par 6 decimals
    {  
      
      fscanf (fic, "%06d", &i); // récupére 6 decimals par 6 decimals
      //printf ("%03d ", i); 
      if(j!=4)
      {
      	somme=i+(int) cle[j]; //decryptage d'un ascii
 		//printf("%c\n",somme);  
 		fprintf(fic2, "%c", somme); //on rempli dans le fichier dest.drt le resultat decodé
 		j=j+1; 
 	  } 
 	  if(j==4)
 	  	j=0; //remise à 0 du compteur cle
    }



	fclose(fic);
	fclose(fic2);
	/*fic2 = fopen("annuaire.drt", "a+");
	fprintf(fic2,"\n");
	fclose(fic2);*/
}

void afficher() // affichage de la fiche
{
	signed char s[256];
	FILE *fichier; /* pointeur sur FILE */
	fichier = fopen("annuaire.drt", "r");  /* read */
	printf("\n");
  	do
	{
		printf("%s",s);
	}while(fgets(s,255,fichier) != NULL);
  	fclose(fichier);
}

int affichageMenu(void) /* pas d'entree donc void */
{
    int choixMenu; /* il manquait un point virgule */
 
    printf("\n\n---Menu---\n\n");
    printf("1.cryptage!\n");
    printf("2.decryptage!\n");
    printf("3.Afficher!\n");
    printf("4.quitter!\n");
    printf("5.Ajouter une fiche!\n");
    printf("6.Recherche dans fichier!\n");
    printf("\nQuel est votre choix?\n");
    fflush (stdout);
    scanf("%d", &choixMenu);
    return choixMenu; /* c'est ici qu'on retourne une valeur*/
}

typedef struct fiche//création de la structure
{
	char nom[256];
	char prenom[256];
	int tel;
}s_fiche;

void ajouter(s_fiche *fiche) //ajout d'une fiche
{
	printf("quel est votre nom?\n");
	scanf("%s",&fiche->nom);
	printf("quel est votre prenom?\n");
	scanf("%s",&fiche->prenom);
	printf("quel est votre numéro de téléphone?\n");
	scanf("%d",&fiche->tel);
	FILE *fichier; /* pointeur sur FILE */
    fichier = fopen("annuaire.txt", "a+");  /* read */
    fseek(fichier, 0, SEEK_SET);
    fprintf(fichier, "%s,%s,%010d;",fiche->nom,fiche->prenom,fiche->tel);
    fclose(fichier);

}

void recherche_fiche_dans_fichier() //recherche dans de fiche dans fichier texte
{
	char chaine[256]="";
	char nom[256]="";
	

	printf("\n\n-------\nLes information de ma fiche sont:\n");
	printf("Quel nom de la personne recherchez vous dans l'annuaire?\n");
	fflush (stdout);
	scanf("%s",nom);
	char *pos;
	

	FILE *fichier; /* pointeur sur FILE */
	fichier = fopen("annuaire.drt", "r");
	char str[256],str2[256];
	while(fgets(str,255,fichier) != NULL)
	{
		/*fscanf(fichier, "%s,",&str2);
    	printf("%s\n",str);
    	printf("%s\n",&str2);*/
	}
	char *p=strtok(str,";");
	while(p != NULL)
  {
    
    char *pos;
		pos = strstr(p, nom);
		if(pos != 0)
		{
			printf("%s\n", p);
		}
    p = strtok(NULL, ";");
  }
    fclose(fichier);
}

int main()
{
	while(1)
	{
		

		switch (affichageMenu()) /* pourquoi choixMenu ca n'esxiste que dans la fonction affichageMenu */
	    {
	    case 1:
	        printf("\nVous avez choisis de crypter!");
	        //system("echo """" > annuaire.crt");
	        cryptage_msg();	
	        break;
	    case 2:
	        printf("\nVous avez choisis de décrypté!");
	        //system("echo """" > annuaire.drt");
			decryptage_msg();
	        break;
	    case 4:
	        printf("\nVous avez choisis de quitter!");
	        return 0;
	        break;
	    case 3:
	    	printf("\nVous avez choisis d'afficher le fichier décrypté!");
	    	afficher();
	        break;
	    case 5:
	    	printf("\nVous avez choisis d'ajouter un nouvelle fiche!");
	    	s_fiche fiche;
	    	ajouter(&fiche);
	        break;
	    case 6:
	    	recherche_fiche_dans_fichier();
	        break;
	    default:
	        printf("\nVous ne ferez rien du tout!");
	        return 0;
	        break;
	    }
	}
	    return 0;

}