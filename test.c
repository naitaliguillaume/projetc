#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void creation_messsage()
{
	 FILE *fichier; /* pointeur sur FILE */
     char message[256];

	/* Créer et remplir le fichier source.txt */
 	 fichier = fopen("source.txt", "w");  /* write */
 	 printf("Quelle est le message à crypter ? (tout en miniscule svp)");
   	 fflush (stdout);
     scanf("%s", &message);
		 if (fichier != NULL)
    {
        fprintf(fichier, "%s\n", message); // Écriture du message
        fclose(fichier);
    }
}


void cryptage_msg()
{
	FILE *fichier;
    FILE *fic;
    //declarations locale:
	int taille_peroq= 4, taille_msg;
	char message[256]="";
	char cle[5];

	/* Recuperer la cle  dans le fichier peroq.def */
 	fichier = fopen("peroq.def", "r");  /* read */

	if (fichier != NULL)
    {
		fgets(cle,5, fichier); // On lit maximum 5 caractères du fichier, on stocke le tout dans "cle"
	}
	fclose(fichier);


	/* Recuperer le msg dans le fichier source.txt */
 	fichier = fopen("source.txt", "r");  /* read */
	if (fichier != NULL)
    {
		// recupère la longueur du msg
		fgets(message,256, fichier); // On lit maximum 256 caractères du fichier, on stocke le tout dans "message"
		taille_msg = strlen(message);
	}
	fclose(fichier);


	int a,b,s=0;
	int j=0;
	//Parcourer du message à aditionner avec le code tournant par exemple abcd
	//ce qui permet le cryptage du message:
	for(int i=0;i<(taille_msg);i++)
	{
		
		if(j<4) // cle de longueur 4
		{
			a =(int)cle[j];
			//printf("%d ",a);
			b =(int)message[i];
			s=(b-a);
			//printf("%d ",b);
			//printf("%d\n",s);
			if(i==0)
			{
				fic = fopen("dest.crt", "w");  /* Ecrit et efface tout le contenu de dest.crt*/
				fprintf(fic,"%03d",s); //affichage sur 3 digit décimal
				fclose(fic);
			}
			else
			{
				fic = fopen("dest.crt", "a+");  /* Ecrit et ajoute du contenu à la suite dans le fichier dest.crt*/
				fprintf(fic,"%03d",s);
				fclose(fic);
			}	

			j=j+1;
			if (j==4)
			{
				j=0; //remise à zéro du conteur position cle
			}

		}
	}
	system("cd /home/guitus/projetc"); //commandes système pour ce mettre dans le repertoire courant
	system("rm source.txt"); //commandes sysstème pour supprimer le fichier source.txt
}

void decryptage_msg()
{
	//déclaration des variables locales:
	FILE *fichier;
    FILE *fic;
    FILE *fic2;
	int taille_peroq= 4;
	int taille_message;
	char message[256];
	char cle[5];
	system("cd /home/guitus/projetc");
	system("echo """" > dest.drt");

	/* Recuperer la cle  dans le fichier peroq.def */
 	fichier = fopen("peroq.def", "r");  /* read */

	if (fichier != NULL)
    {
		fgets(cle,5, fichier); // On lit maximum 5 caractères du fichier, on stocke le tout dans "cle"
	}
	fclose(fichier);


 	fic = fopen("dest.crt", "r");  /* lecture du fichier crypté*/
 	fic2 = fopen("dest.drt", "a+");  /* ouverture du fichier décrypté pour le remplir du message décrypté*/
 	int i=0; //variable de parcour du fichier crypté
 	int j=1; //variable de parcour de la cle
 	int somme=0; //varial pour le décryptage

 	fscanf (fic, "%03d", &i); // récupére 3 decimals par 3 decimals
 	//printf ("%03d ", i);   
 	somme=i+(int) cle[0]; // calcul du decryptatge
 	printf("%c\n",somme); 
 	fprintf(fic2, "%c", somme);
  	while (!feof (fic)) // parcour du fichier 3 decimals par 3 decimals
    {  
      
      fscanf (fic, "%03d", &i); // récupére 3 decimals par 3 decimals
      //printf ("%03d ", i); 
      if(j!=4)
      {
      	somme=i+(int) cle[j]; //decryptage d'un ascii
 		printf("%c\n",somme);  
 		fprintf(fic2, "%c", somme); //on rempli dans le fichier dest.drt le resultat decodé
 		j=j+1; 
 	  } 
 	  if(j==4)
 	  	j=0; //remise à 0 du compteur cle
    }



	fclose(fic);
	fclose(fic2);
}

int main()
{
	creation_messsage();
	cryptage_msg();	
	decryptage_msg();
	return 0;
}